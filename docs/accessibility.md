# Accessibility

W3C publishes [Web Content Accessibility Guidelines (WCAG)](https://www.w3.org/WAI/standards-guidelines/wcag/) which we should aim to follow as closely as possible.
The [BBC guidelines](https://www.bbc.co.uk/guidelines/futuremedia/accessibility/) are also useful, and well worth reading.

## ARIA

ARIA attributes are designed to give meaning to markup elements.
They are a critical step to ensuring that our products can be used by the widest audience possible, for example those users who access the web via a screen reader.

Try to avoid using generic links which do not describe the text being linked to, such as "Read More".
Where unavoidable, use `aria-label` to provide a more verbose description of the link contents, or `aria-labelledby` to reference a more descriptive element.
While the title attribute might seem logical, it is ignored by screen readers.

```html
<h1 id="brown-fox-title">DevOps at Beautiful Canoe</h1>

<!-- Screen reader says: "Link; Article about the DevOps at Beautiful Canoe" -->
<a href="#" aria-label="Article about DevOps at Beautiful Canoe">Read more</a>

<!-- Screen reader says: "Link; DevOps at Beautiful Canoe" -->
<a href="#" aria-labelledby="brown-fox-title">Read more</a>
```

## Images

The HTML `alt` attribute on images is required, and should provide a concise summary of the image contents.
A more verbose description can be added using `aria-describedby`, which is especially useful if the image has an associated caption.

## Colours

The [colour palette](/colours.html) here was produced with [Adobe Color](https://color.adobe.com/) and based on the lake photograph on the Beautiful Canoe website.
Any changes to the palette should be checked with a tool such as [Colorblinding](https://chrome.google.com/webstore/detail/colorblinding/dgbgleaofjainknadoffbjkclicbbgaa?hl=en) (for Chrome) or [Let's Go Color Blind](https://addons.mozilla.org/en-GB/firefox/addon/let-s-get-color-blind/) (for Firefox).

## Tooling

Resources exist to guide the developer through a straightforward accessibility audit.
[Wave](https://wave.webaim.org/) can be installed as a browser plugin and run regularly in the developer environment.
On a regular basis, a [Google Lighthouse](https://developers.google.com/web/tools/lighthouse) audit should be run by the developer.
This can also be added to the CI/CD pipeline, to provide regular feedback on the production environment:

```yaml
audit:lighthouse:
    image: markhobson/node-chrome
    stage: audit
    only:
        - schedules
    variables:
        LIGHTHOUSE_TEST_URL: https://URL...
    before_script:
        - npm i -g lighthouse
    script:
        - lighthouse --chrome-flags="--headless --no-sandbox" $LIGHTHOUSE_TEST_URL --max-wait-for-load 300000--output html --output-path ./report.html
    artifacts:
        paths:
            - ./report.html
        expire_in: 1 week
```

The schedule for this CI job should be reasonable conservative, say, weekly at most, as the audit itself takes a reasonably long time to run.
