# Photography

Photography used on Beautiful Canoe sites and literature should reflect the [attitudes and values of the company](index.md#our-attitudes-and-values).

## Our look and feel

Photography for Beautiful Canoe literature should feature real people and real businesses.
**Stock images should be avoided.**
Photography should capture the energy that is needed to run a small company, in a way that communicates approachability.
The imagery should make people think "if they can do it, so can I".

Photography can feature one or more people, and can be cropped in various ways.
Subjects may be looking straight into camera or focused on a point off camera.
Ideally, a reportage style of photography should be used, capturing glimpses of real people at work.

Our imagery should emphasise:

* diversity of age, gender, ability and ethnicity;
* authenticity (avoid staged scenes);
* joy;
* action and movement, and
* collaboration.

## Image formats

* Web Format -- `750px` or `500px` wide (`361mm` or `176mm`) @ `72dpi`, jpg or png
* Print Format -- `4961px` x `3720px` (`420mm` x `315mm`) @ `300dpi`, jpg or TIFF
* Large Format -- `7874px` x `5906px` (`2000mm` x `1500mm`) @ `100dpi`, jpg or TIFF

## Responsiveness

Any image that is more than a couple of hundred pixels wide should be responsive.
Use CSS to create two standard classes, one for images of `750px` wide and one for `500px` wide images.
Each class should have a variant for viewports that are equal to or larger than the image size, and one for viewports smaller than the image width:

```css
@media all and (max-width: 749px) {
    .responsive750 {
        width: 100%;
    }
}

@media all and (min-width: 750px) {
    .responsive750 {
        width: 750px;
    }
}

@media all and (max-width: 499px) {
    .responsive500 {
        width: 100%;
    }
}

@media all and (min-width: 500px) {
    .responsive500 {
        width: 500px;
    }
}
```

Then, rather than use a fixed image width in the `<img>` element, resize the image in [Gimp](https://www.gimp.org/) or similar, to have a width of either `750px` or `500px`, then use the appropriate responsive image class defined above:

```html
<div class="center">
    <img src="images/developers/2016-retro.jpg" class="responsive750" alt="Beautiful Canoe software developers running a retrospective."/>
</div>
```

## Look and feel examples

![Team member portrait example - Gareth](./figures/gareth.jpg)
![Team member portrait example - David](./figures/david.jpg)
![Group shot look and feel example - 2016 retrospective](./figures/2016-retro.jpg)
![Group shot look and feel example - 2017 coaching session](./figures/2017-coaching.jpg)
![Group shot look and feel example - 2017 whiteboard](./figures/2017-whiteboard.jpg)
