# Colours

Our conventions for using colour are based on the [Google material design guidelines](https://material.io/design/color/#color-usage-palettes).
The palette consists of one primary and one secondary colour, which both have light and dark variants.

!!!Tip
    Click on the colour tiles to copy HEX or RGB values to the clipboard.

## Primary palette

The primary palette should appear most frequently in any Beautiful Canoe communications.
It consists of one primary brand colour, <span style="background-color: #795548; color: white; padding: 3px;">&nbsp;brown&nbsp;</span>, with light and dark variants.

<div style="text-align: center;">
    <button class="copybtn" data-clipboard-text="#795548" style="background-color: #795548; color: white; width: 8em; height: 8em;"><code>#795548</code></button>
    <button class="copybtn" data-clipboard-text="#a98274" style="background-color: #a98274; color: white; width: 8em; height: 8em;"><code>#a98274</code></button>
    <button class="copybtn" data-clipboard-text="#4b2c20" style="background-color: #4b2c20; color: white; width: 8em; height: 8em;"><code>#4b2c20</code></button>
</div>

### RGB values

<div style="text-align: center;">
    <button class="copybtn" data-clipboard-text="rgb(121, 85, 72)" style="background-color: #795548; color: white; width: 8em; height: 8em;"><code>121, 85, 72</code></button>
    <button class="copybtn" data-clipboard-text="rgb(169, 130, 116)" style="background-color: #a98274; color: white; width: 8em; height: 8em;"><code>169, 130, 116</code></button>
    <button class="copybtn" data-clipboard-text="rgb(75, 44, 32)" style="background-color: #4b2c20; color: white; width: 8em; height: 8em;"><code>75, 44, 32</code></button>
</div>

### Pantone<sup>&reg;</sup> matches (with white and black)

![Pantone&reg; matches: primary palette](figures/pantones-primary.jpg)

## Secondary palette

The secondary palette should be used sparingly, and only on elements that should be accented.
Secondary colours are most often used for:

* Links and headlines
* Buttons
* Selection controls, like sliders and switches
* Highlighting selected text
* Progress bars

The main secondary brand colour is <span style="background-color: #8c0e03; color: white; padding: 3px;">&nbsp;red&nbsp;</span>, with light and dark variants.

<div style="text-align: center;">
    <button class="copybtn" data-clipboard-text="#8c0e03" style="background-color: #8c0e03; color: white; width: 8em; height: 8em;"><code>#8c0e03</code></button>
    <button class="copybtn" data-clipboard-text="#c3452d" style="background-color: #c3452d; color: white; width: 8em; height: 8em;"><code>#c3452d</code></button>
    <button class="copybtn" data-clipboard-text="#590000" style="background-color: #590000; color: white; width: 8em; height: 8em;"><code>#590000</code></button>
</div>

### RGB values

<div style="text-align: center;">
    <button class="copybtn" data-clipboard-text="rgb(140, 14, 3)" style="background-color: #8c0e03; color: white; width: 8em; height: 8em;"><code>140, 14, 3</code></button>
    <button class="copybtn" data-clipboard-text="rgb(195, 69, 45)" style="background-color: #c3452d; color: white; width: 8em; height: 8em;"><code>195, 69, 45</code></button>
    <button class="copybtn" data-clipboard-text="rgb(89, 0, 0)" style="background-color: #590000; color: white; width: 8em; height: 8em;"><code>89, 0, 0</code></button>
</div>

### Pantone<sup>&reg;</sup> matches (with white and black)

![Pantone&reg; matches: secondary palette](figures/pantones-secondary.jpg)

## Monochrome palette

<div style="text-align: center;">
    <button class="copybtn" data-clipboard-text="#f6f6f6" style="background-color: #f6f6f6; color: white; width: 8em; height: 8em; border: 1px solid black;"><code>#f6f6f6</code></button>
    <button class="copybtn" data-clipboard-text="#bebebe" style="background-color: #bebebe; color: white; width: 8em; height: 8em; border: 1px solid black;"><code>#bebebe</code></button>
    <button class="copybtn" data-clipboard-text="#818181" style="background-color: #818181; color: black; width: 8em; height: 8em;"><code>#818181</code></button>
    <button class="copybtn" data-clipboard-text="#444444" style="background-color: #444444; color: white; width: 8em; height: 8em;"><code>#444444</code></button>
    <button class="copybtn" data-clipboard-text="#272727" style="background-color: #272727; color: black; width: 8em; height: 8em;"><code>#272727</code></button>
</div>

### RGB values

<div style="text-align: center;">
    <button class="copybtn" data-clipboard-text="rgb(246, 246, 246)" style="background-color: #f6f6f6; color: white; width: 8em; height: 8em; border: 1px solid black;"><code>246, 246, 246</code></button>
    <button class="copybtn" data-clipboard-text="rgb(190, 190, 190)" style="background-color: #bebebe; color: white; width: 8em; height: 8em; border: 1px solid black;"><code>190, 190, 190</code></button>
    <button class="copybtn" data-clipboard-text="rgb(129, 129, 129)" style="background-color: #818181; color: black; width: 8em; height: 8em;"><code>129, 129, 129</code></button>
    <button class="copybtn" data-clipboard-text="rgb(68, 68, 68)" style="background-color: #444444; color: white; width: 8em; height: 8em;"><code>68, 68, 68</code></button>
    <button class="copybtn" data-clipboard-text="rgb(39, 39, 39)" style="background-color: #272727; color: black; width: 8em; height: 8em;"><code>39, 39, 39</code></button>
</div>

### Pantone<sup>&reg;</sup> matches

![Pantone&reg; matches: monochrome palette](figures/pantones-monochrome.jpg)

## Further reading

* [Adobe color tool](https://color.adobe.com/)
* [Google material design colour guidelines](https://material.io/design/color/#color-usage-palettes)
* [Material design colour tool](https://material.io/resources/color/#!/?view.left=0&view.right=0&primary.color=795548&secondary.color=8c0e03)
