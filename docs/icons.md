# Icons

Beautiful Canoe uses [https://fontawesome.com/](https://fontawesome.com/) icons, version 5 or above.

## License

The free Font Awesome set is licensed by the [CC By 4.0 License](https://creativecommons.org/licenses/by/4.0/).

## Load icons

To load Font Awesome icons, include the following in the `<head>` of your HTML page:

```html
<!-- Latest Font Awesome kit. -->
<script src="https://kit.fontawesome.com/36462bdc6f.js"></script>
```

## Styling icons

Icons should be styled in the [primary brand colour](colours.md), similarly to this:

```css
.icon-container {
    font-size: 2em;
    display: flex;
    text-align: center;
    vertical-align: middle;
}

i.fas {
    color: $primary;
    height: 2em;
    width: 2em;
    padding: 5px;
    margin: 5px;
    line-height: inherit;
}

i.fas:hover {
    background: $primary;
    color: $white;
}
```

## Using an icon in HTML

```html
<div class="icon-container">
    <i class="fas fa-ICON-NAME fa-1x" title="TITLE"></i>
</div>
```

## Value and attitude icons

The standard set of icons for the Beautiful Canoe [attitudes and values](index.md#our-attitudes-and-values) can be viewed / downloaded from the [Font Awesome site](https://fontawesome.com/):

### Values

* :fontawesome-solid-compass-drafting: [Craft](https://fontawesome.com/icons/drafting-compass?style=solid)
* :fontawesome-solid-hand-holding-heart: [Joy](https://fontawesome.com/icons/hand-holding-heart?style=solid)
* :fontawesome-solid-infinity: [Flow](https://fontawesome.com/icons/infinity?style=solid)
* :fontawesome-solid-bolt: [Edge](https://fontawesome.com/icons/bolt?style=solid)
* :fontawesome-solid-hands-holding: [Openness](https://fontawesome.com/icons/hands?style=solid)
* :fontawesome-solid-handshake-angle: [Collaboration](https://fontawesome.com/icons/hands-helping?style=solid)

### Attitudes

* :fontawesome-solid-lightbulb: [Innovative and inspiring](https://fontawesome.com/icons/lightbulb?style=solid)
* :fontawesome-solid-door-open: [Approachable](https://fontawesome.com/icons/door-open?style=solid)
* :fontawesome-solid-user-check: [Professional](https://fontawesome.com/icons/user-check?style=solid)
* :fontawesome-solid-book: [Knowledgeable](https://fontawesome.com/icons/book?style=solid)
* :fontawesome-solid-shuffle: [Adaptable](https://fontawesome.com/icons/random?style=solid)
* :fontawesome-solid-calendar-check: [Competent](https://fontawesome.com/icons/calendar-check?style=solid)
