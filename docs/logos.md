# Logos

These files are large, and will need to be resized.

## White background

![Beautiful Canoe logo with white background](./figures/B_Canoe_300dpi_RGB.jpg)

## Transparent background

![Beautiful Canoe logo with transparent background](./figures/B_Canoe_300dpi_RGB.png)

## Black

![Beautiful Canoe logo in black with transparent background](./figures/B_Canoe_black_300dpi.png)

## White

<img src="./figures/B_Canoe_white_300dpi.png" alt="Beautiful Canoe logo in white with transparent background"
    title="Beautiful Canoe logo in white with transparent background" style="background: black;" />

## Square

![Beautiful Canoe logo square](./figures/square-wood-rgb.png)

## Favicon

If you need one 48x48px favicon, use this:

![Beautiful Canoe favicon for internal sites](./figures/favicon.ico)

For more heavyweight uses, unzip [this zip file](./figures/favicons.zip) and add the following mark-up to your header:

```html
<link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<link rel="manifest" href="/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
```

Note that depending on where you place the favicons, you may need to change the URLs above, and those in `manifest.json` within the `.zip` file.
