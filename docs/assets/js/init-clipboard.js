var btns = document.querySelectorAll('button');
var clipboard = new ClipboardJS(btns);

clipboard.on('success', function(event) {
    console.log(event);
    var div = document.createElement('div');
    div.setAttribute('class', 'alert alert-success');
    div.setAttribute('role', 'alert');
    var button = document.createElement('button');
    button.setAttribute('class', 'closebtn closebtn-success');
    button.setAttribute('role', 'button');
    button.setAttribute('onclick', "this.parentElement.style.display='none';");
    button.setAttribute('data-dismiss', 'alert');
    button.setAttribute('aria-label', 'Close');
    button.innerHTML = "<span aria-hidden=\"true\">&times;</span>";
    div.appendChild(button);
    div.innerHTML = div.innerHTML.concat("<span>Copied <strong>", event.text, "</strong> to clipboard.</span>");
    event.trigger.parentNode.before(div);
});

clipboard.on('error', function(event) {
    console.log(event);
    var div = document.createElement('div');
    div.setAttribute('class', 'alert alert-error');
    div.setAttribute('role', 'alert');
    var button = document.createElement('button');
    button.setAttribute('class', 'closebtn closebtn-error');
    button.setAttribute('role', 'button');
    button.setAttribute('onclick', "this.parentElement.style.display='none';");
    button.setAttribute('data-dismiss', 'alert');
    button.setAttribute('aria-label', 'Close');
    button.innerHTML = "<span aria-hidden=\"true\">&times;</span>";
    div.appendChild(button);
    div.innerHTML = div.innerHTML.concat("<span>Could not copy to clipboard.</span>");
    event.trigger.parentNode.before(div);
});

// Bind ESC key to close all alerts.
document.addEventListener('keyup', (evt) => {
    if(evt.keyCode === 27) {
        document.querySelectorAll('[role="alert"]').forEach(function (elem) {
            elem.style.display = "none";
        });
    }
});
