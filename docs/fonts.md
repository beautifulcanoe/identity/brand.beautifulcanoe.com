# Fonts

Beautiful Canoe branded fonts are [Didact Gothic](https://fonts.google.com/specimen/Didact+Gothic), for ordinary text:

> ABCČĆDĐEFGHIJKLMNOPQRSŠTUVWXYZŽabcčćdđefghijklmnopqrsštuvwxyzž1234567890‘?’“!”(%)#{@}/&\<-+÷×=>®©$€£¥¢:;,.*

and [Source Code Pro](https://fonts.google.com/specimen/Source+Code+Pro) for code:

> `ABCČĆDĐEFGHIJKLMNOPQRSŠTUVWXYZŽabcčćdđefghijklmnopqrsštuvwxyzž1234567890‘?’“!”(%)#{@}/&\<-+÷×=>®©$€£¥¢:;,.*`

## Licenses

[Didact Gothic](https://fonts.google.com/specimen/Didact+Gothic?selection.family=Didact+Gothic) and [Source Code Pro](https://fonts.google.com/specimen/Source+Code+Pro) are both licensed under the [SIL Open Font License, Version 1.1](http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL).

## Embed Font

[Google Fonts](https://fonts.google.com/) provides three mechanisms for including a typeface in a site: with a link element, a CSS `@import` rule, or with JavaScript.
By convention we use [Web Font Loader](https://github.com/typekit/webfontloader) to load only the subsets of the fonts we need.

The following example loads the Beautiful Canoe brand fonts at the required font-weights (300, 400, 700), and the Latin subset.
If glyphs from other subsets will be required, you will need to from the `latin` argument.

```html
<!-- Web Font Loader licensed by the Apache License v2.0 https://github.com/typekit/webfontloader -->
<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script>
<!-- Didact Gothic licensed by SIL OFL v1.1. See: https://github.com/ossobuffo/didact-gothic -->
<!-- Source Code Pro licensed by SIL OFL v1.1. See: https://github.com/adobe-fonts/source-code-pro -->
<script>
WebFont.load({
    google: {
        families: ['Didact+Gothic:300,400,700:latin',
                   'Source+Code+Pro:300,400,700:latin',]
    }
});
</script>
```

## Specify in CSS

Use the following CSS rules to specify these families in CSS or SASS:

```css
font-family: 'Source Code Pro', monospace;
font-family: 'Didact Gothic', sans-serif;
```

## Font weights

Ensure that the font-weight property is defined using a numerical value rather than a keyword, and that the weight you are after is being loaded:

* 100 - N/A
* 300 - N/A
* <span style="font-weight: 400">400 - normal</span>
* 600 - N/A
* <span style="font-weight: 700">700 - bold</span>

## Font sizes

Online text should not have fixed font sizes, but instead should use *typography scaling*.
With a typographic scale, each font size is a fixed ratio larger than two font sizes either side of it.

See [Further Reading](#further-reading) for a selection of articles on typographic scaling.

```scss
:root {
    $interval: 1.2;    // Unitless for proportional, unit for fixed.
    $body-text: 1rem;  // Must have a unit.
    $scale-min: -2;
    $scale-max: 4;

    --int: #{$interval};
    --scale0: #{$body-text};

    @if $scale-min < 0 {
        // Generate scale variables smaller than the base text size.
        @for $i from -1 through $scale-min {
            @if type-of($interval) == number {
                @if unitless($interval) {
                    --scale#{$i}: calc(var(--scale#{$i + 1}) / var(--int));
                } @else {
                    --scale#{$i}: calc(var(--scale#{$i + 1}) - var(--int));
                }
            }
        }
    }
    @if $scale-max > 0 {
        // Generate scale variables larger than the base text size.
        @for $i from 1 through $scale-max {
            @if type-of($interval) == number {
                @if unitless($interval) {
                    --scale#{$i}: calc(var(--scale#{$i - 1}) * var(--int));
                } @else {
                    --scale#{$i}: calc(var(--scale#{$i - 1}) + var(--int));
                }
            }
        }
    }
}
```

The interval between font sizes should be dependent on the size of the screen.
The larger the screen size, the larger the interval between font sizes:

```scss
@media (min-width: 22em) {
    :root {
        --int: 1.1
    }
}
@media (min-width: 32em) {
    :root {
        --int: 1.4
    }
}
@media (min-width: 42em) {
    :root {
        --int: 1.6
    }
}
```

## Microsoft equivalent fonts

The TTF version of [Didact Gothic](https://fonts.google.com/specimen/Didact+Gothic) can be downloaded [here](https://github.com/ossobuffo/didact-gothic/tree/master/fonts).
[Souce Code Pro](https://fonts.google.com/specimen/Source+Code+Pro) has TTF versions of all its variants [here](https://github.com/adobe-fonts/source-code-pro/tree/release/TTF).

## Further reading

* [A typographic scale](http://spencermortensen.com/articles/typographic-scale/)
* [A Modern typographic Scale by Rob Weychert](https://24ways.org/2019/a-modern-typographic-scale/)
* [Google Fonts](https://fonts.google.com/)
* [Typographic Scale calculator](https://type-scale.com/)
* [Web Font Loader](https://github.com/typekit/webfontloader)
