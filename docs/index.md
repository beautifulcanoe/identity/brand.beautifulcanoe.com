# Beautiful Canoe Brand Guidelines

*Empowerment through craft and challenge.*

## Introduction

These guidelines cover every aspect of the Beautiful Canoe brand including logo use, colours and typography.

## Our attitudes and values

We are:

* :fontawesome-solid-lightbulb: *Innovative and inspiring* - We use mature methods and technologies for reliability and surety; however, we are creative and use innovative approaches to problem solving.
* :fontawesome-solid-door-open: *Approachable* - We ensure that the experience that our clients and staff have with Beautiful Canoe is supportive, encouraging and pleasant.
* :fontawesome-solid-user-check: *Professional* - We ensure that the interactions, methods and processes we use are of a high standard.
* :fontawesome-solid-book: *Knowledgeable* - We draw on the best and most relevant knowledge that our staff and the academic community can offer.
* :fontawesome-solid-shuffle: *Adaptable* - We are flexible and responsive to client needs.
* :fontawesome-solid-calendar-check: *Competent* - We ensure that the client receives products and services that are of high quality, effective and robust.

Our business is based around these values:

* :fontawesome-solid-compass-drafting: *Craft* - We see our software development as our craft. We value craftsmanship and attention to quality.
* :fontawesome-solid-hand-holding-heart: *Joy* - We are here because we enjoy and value what we do, and because it makes us happy.
* :fontawesome-solid-infinity: *Flow* - We believe that we do our best work when we achieve a state of flow. We are deeply immersed in what we do, and see our adaptivity as natural state of being.
* :fontawesome-solid-bolt: *Edge* - We embrace challenge, acknowledging that challenging oneself is often uncomfortable. We believe that this is when we learn the most.
* :fontawesome-solid-hands-holding: *Openness* - We believe in transparency, honesty and the sharing of knowledge. We acknowledge the massive impact free and open source software has had both on the world and our work, and aim to contribute wherever we can.
* :fontawesome-solid-handshake-angle: *Collaboration* - We work with our clients and involve them in the development and delivery process to find the most effective solutions.
