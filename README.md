# Beautiful Canoe Brand Guidelines

[![pipeline status](https://gitlab.com/beautifulcanoe/identity/brand.beautifulcanoe.com/badges/main/pipeline.svg)](https://gitlab.com/beautifulcanoe/identity/brand.beautifulcanoe.com/-/commits/main)

Brand guidelines for the Beautiful Canoe website and other marketing material.

## Making changes to Beautiful Canoe brand guidelines

Please see [CONTRIBUTING.md](/CONTRIBUTING.md) for advice on how to contribute to this repository.

## License

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a>

This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

